# Drag-Crash
宫格滑块拼图小游戏 4.0 （纯原生JS）

已完成难度：

3 × 3 九宫格
4 × 4 十六宫格
5 × 5 二十五宫格

游戏规则：

从上到下，从左到右，按数字顺序排列复原即算完成。

温馨提示：

若拼到最后出现一组数字的位置是对调情况，则该局称为“死局”，不可还原情况，请重新开始。