let times = 0; // 计时时间
let timer = null; // 计时器
let pause = true; // 开始/暂停
let targetIndex = null; // 目标位置下标
let board = []; // 板面位置下标（固定）
let block = []; // 滑块编号（动态）
let direction = []; // 可移动的目标位置下标（接收二维数组）
let xy = []; // 板面位置的坐标值（接收二维数组）

// DOM
let $gameWrap = null; // 游戏区域
let $startOrPause = null; // 按钮组
let $timerWrap = null; // 计时器
let $dialogWrap = null; // 对话框
let $dialogContent = null; // 对话框内容

/**
 * 格式化时间
 * @params {String} type 返回类型
 */
function formatTimes(type) {
    // 转换分秒
    let minute = parseInt(times / 60) < 10 ? "0" + parseInt(times / 60) : parseInt(times / 60);
    let second = times % 60 < 10 ? "0" + times % 60 : times % 60;
    // 转换类型
    if (type === 'chart') {
        return minute + ":" + second;
    } else if (type === 'cn') {
        return minute + "分" + second + "秒";
    }
}

/**
 * 开始/暂停
 */
function play() {
    if (pause) {
        pause = false;
        $startOrPause.innerText = "暂停";
        timer = setInterval(function () {
            $timerWrap.innerText = formatTimes('chart');
            times += 1;
        }, 1000);
    } else {
        pause = true;
        $startOrPause.innerText = "开始";
        $dialogContent.innerHTML = '哦吼！完成拼图，游戏结束<br>总耗时：' + formatTimes('cn');
        clearInterval(timer);
    }
}

/**
 * 对话框 - 显示
 */
function showDialog() {
    $dialogWrap.style.transform = "translate(-50%, -50%) scale(1)";
}

/**
 * 对话框 - 隐藏
 */
function hideDialog() {
    $dialogWrap.style.transform = "translate(-50%, -50%) scale(0)";
}

/**
 * 移动滑块
 * @params {Number} id 滑块的编号id
 * @params {Number} index 移动模板坐标值的下标
 */
function moveBlock(id, index) {
    document.getElementById("d" + id).style.top = xy[index][0] + "px";
    document.getElementById("d" + id).style.left = xy[index][1] + "px";
}

/**
 * 点击滑块
 * @params {Number} id 滑块的编号id
 */
function clickBlock(id) {
    // 若游戏暂停/停止，点击滑块不作任何操作
    if (pause) return;
    // 获取点击滑块所在的位置下标
    let numIndex = block.indexOf(id);
    if (direction[numIndex].includes(targetIndex)) {
        // 移动滑块
        moveBlock(id, targetIndex);
        // 交换滑块与空位位置
        let temp = block[numIndex];
        block[numIndex] = block[targetIndex];
        block[targetIndex] = temp;
        // 更新空位的下标
        targetIndex = numIndex;
        // 对比是否完成拼图
        if (block.join() === board.join()) {
            play();
            showDialog();
        }
    }
}

/**
 * 是否僵局
 */
function idStandOff() {

}

/**
 * 重置
 */
function reset() {
    // 洗牌函数
    for (let i = block.length - 1; i >= 0; i--) {
        let r = Math.floor(Math.random() * (i + 1));
        let temp = block[r];
        block[r] = block[i];
        block[i] = temp;
        if (temp === 0) {
            targetIndex = i;
            continue;
        }
        moveBlock(block[i], i);
    }
    times = 0;
    pause && play();
}

/**
 * 选择游戏难度
 * @params {Number} level 游戏难度
 */
function chooseLevel(level) {
    // 更新全局当前游戏难度与数据
    $currentLevel = level;
    board = $gameData[level].board;
    block = $gameData[level].block;
    direction = $gameData[level].direction;
    xy = $gameData[level].xy;
    // 更新游戏板面布局
    for (let i = 0; i < $gameDataLeves.length; i++) {
        let val = $gameDataLeves[i];
        if (val === level) {
            $gameWrap.classList.add("game-wrap-" + val);
        } else {
            $gameWrap.classList.remove("game-wrap-" + val);
        }
    }
    $gameWrap.innerHTML = $gameData[level].blockDOM;
    reset();
}

/**
 * 初始化加载
 */
window.onload = function () {
    $gameWrap = document.getElementById("gameWrap");
    $startOrPause = document.getElementById("startOrPause");
    $timerWrap = document.getElementById("timerWrap");
    $dialogWrap = document.getElementById("dialogWrap");
    $dialogContent = document.getElementById("dialogContent");
    chooseLevel($currentLevel);
}